<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Category_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'category';
		$this->idx	 = 'cat_idx';
		$this->fields	 = array(
			'cat_type' => array('Type', TRUE),
			'cat_name' => array('Category', TRUE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($idx = FALSE)
	{
		return parent :: save ($idx);	
	}

	/**
	 * Get record category with filtering cat_type
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	*/
	public function get_category_name ($type = FALSE, $source = FALSE, $itcat_idx = FALSE)
	{
		if(!$source)
		{
			$this->db->where('cat_type', $type);
			$this->db->order_by('cat_name');
			return parent :: get ();
		}
		else
		{
			if ($itcat_idx)
			{
				for($i=0; $i<count($itcat_idx); $i++)
				    $idx[] = $itcat_idx[$i]->cat_idx;
				$this->db->where_not_in('cat_idx', $idx);
			}
			$this->db->order_by('cat_name');
			return parent :: get ();
		}
	}
}