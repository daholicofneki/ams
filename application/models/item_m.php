<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Item_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'item';
		$this->idx 	 = 'it_code';
		$this->fields	 = array(
			'it_code' => array ('Item kode', TRUE),
			'it_name' => array('Name', TRUE),
			'it_desc' => array('Description', TRUE),
			'it_has_ed' => array('Has ED', FALSE),
			'it_type' => array('Item Type', TRUE),
			'it_qty' => array('Qty', TRUE),
			'it_unit' => array('Unit', FALSE),
			'it_produced_by' => array('Produced by', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($idx = FALSE)
	{
		if ($this->input->post('cats'))
		{
			foreach ($this->input->post('cats') as $key => $val)
			{
				$this->db->set('it_code', $this->input->post('it_code'));
				$this->db->set('cat_idx', $val);
				$this->db->insert('item_category');
			}
		}

		if ($this->input->post('it_price') > 0)
		{
			$this->db->set('it_code', $this->input->post('it_code'));
			$this->db->set('ip_date_from', date('Y-m-d'));
			$this->db->set('ip_price', $this->input->post('it_price'));
			$this->db->insert('item_price');
		}

		parent :: save ($idx);
		return TRUE;
	}

	/**
	 * Insert category item
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function insert_category ($it_code = FALSE)
	{
		if ($this->input->post('cats'))
		{
			foreach ($this->input->post('cats') as $key => $val)
			{
				$this->db->set('it_code', $it_code);
				$this->db->set('cat_idx', $val);
				$this->db->insert('item_category');
			}
		}
		return TRUE;
	}

	/**
	 * Insert price item
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function insert_price ($it_code = FALSE)
	{
		# Update column last valid price ip_date_from
		

		# Inser new valid price
		$this->db->set('it_code', $it_code);
		$this->db->set('ip_date_from', $this->input->post('ip_date_from'));
		$this->db->set('ip_price', $this->input->post('it_price'));
		$this->db->insert('item_price');
		return TRUE;
	}

	/**
	 * Update price item
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function update_price ($it_code = FALSE, $ip_idx = FALSE)
	{
		return TRUE;
	}
}