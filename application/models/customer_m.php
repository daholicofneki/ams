<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Customer Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Customer_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'customer';
		$this->idx	 = 'cus_idx';
		$this->fields	 = array(
			'cus_name' => array('Name', TRUE),
			'cus_pic' => array('Contact Person', FALSE),
			'cus_phone' => array('Phone', FALSE),
			'cus_handphone' => array('Handphone', FALSE),
			'cus_fax' => array('Fax', FALSE),
			'cus_email' => array('Email', FALSE),
			'cus_address' => array('Address', FALSE),
			'cus_city' => array('City', FALSE),
			'cus_remark' => array('Remark', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($idx = FALSE)
	{
		return parent :: save ($idx);	
	}
}