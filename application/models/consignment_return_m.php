<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Consignment_return_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'consignment_return';
		$this->idx	 = 'rcon_code';
		$this->fields	 = array(
			'rcon_code' => array('Return No', TRUE),
			'con_code' => array('Ref No', TRUE),
			'rcon_date' => array('Return Date', TRUE),
			'sp_idx' => array('Supplier Idx', TRUE),
			'rcon_sp_name' => array('Supplier Name', TRUE),
			'rcon_discount' => array('Discount', TRUE),
			'rcon_vat' => array('Vat', TRUE),
			'rcon_sent_by' => array('Sent By', TRUE),
			'rcon_received_by' => array('Received By', TRUE),
			'rcon_last_updated_time' => array('Last updated', FALSE),
			'rcon_last_updated_by' => array('Last updated by', FALSE),
			'rcon_remark' => array('Remark', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($code = FALSE)
	{
		if ($this->input->post('it_code'))
		{
			foreach ($this->input->post('it_code') as $key => $val)
			{
				$this->db->set('it_code', $this->input->post('it_code'));
				$this->db->set('rcon_code', $code);
				$this->db->set('rconit_qty', $this->input->post('it_qty'));
				$this->db->set('rconit_price', $this->input->post('it_price'));
				$this->db->set('rconit_remark', $this->input->post('it_remark'));
				$this->db->insert('consignment_return_item');
			}
		}
		parent :: save ($idx);
		return TRUE;
	}

	/**
	 * Get PO Code
	 *
	 * @access	public
	 * @return	integer
	 */
	public function get_current_con_code ()
	{
		$this->db->select('MAX(pi_no)');
		$max_record = $this->one($this->tableName);
		$INIT = substr ($max_record, 0, 2);
		$ID = substr ($max_record, 2, 4);
		if ($INIT == 'IP')
		{
			return $INIT.str_pad($ID+1, 4, "0", STR_PAD_LEFT);
		}
		else
		{
			return 'IP0001';	
		}
	}
}