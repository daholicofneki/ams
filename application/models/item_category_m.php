<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Item_category_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'item_category';
		$this->idx	 = 'itcat_idx';
		$this->fields	 = array(
			'it_code' => array('Code Item', TRUE),
			'cat_idx' => array('Idx Category', TRUE)
		);
	}

	/**
	 * Get record category with filtering it_code
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	*/
	public function get_category_name ($it_code = FALSE)
	{
		$this->db->join('category', 'category.cat_idx = item_category.cat_idx');
		$this->db->where('it_code', $it_code);
		$this->db->order_by('cat_name');
		return parent :: get ();
	}
}