<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Supplier_con_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'supplier_con';
		$this->idx	 = 'sp_idx';
		$this->fields	 = array(
			'sp_name' => array('Name', TRUE),
			'sp_pic' => array('Contact Person', FALSE),
			'sp_phone' => array('Phone', FALSE),
			'sp_handphone' => array('Handphone', FALSE),
			'sp_fax' => array('Fax', FALSE),
			'sp_email' => array('Email', FALSE),
			'sp_address' => array('Address', TRUE),
			'sp_city' => array('City', TRUE),
			'sp_remark' => array('Remark', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($idx = FALSE)
	{
		return parent :: save ($idx);	
	}
}