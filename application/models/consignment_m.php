<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Consignment_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'consignment';
		$this->idx	 = 'con_code';
		$this->fields	 = array(
			'con_code' => array('Consignment No', TRUE),
			'con_date' => array('Consignment Date', TRUE),
			'sp_idx' => array('Supplier Idx', TRUE),
			'con_sp_name' => array('Supplier Name', TRUE),
			'con_discount' => array('Discount', TRUE),
			'con_vat' => array('Vat', TRUE),
			'con_sent_by' => array('Sent By', TRUE),
			'con_received_by' => array('Received By', TRUE),
			'con_last_updated_time' => array('Last updated', FALSE),
			'con_last_updated_by' => array('Last updated by', FALSE),
			'con_remark' => array('Remark', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($code = FALSE)
	{
		if ($this->input->post('it_code'))
		{
			foreach ($this->input->post('it_code') as $key => $val)
			{
				$this->db->set('it_code', $this->input->post('it_code'));
				$this->db->set('con_code', $code);
				$this->db->set('conit_qty', $this->input->post('it_qty'));
				$this->db->set('conit_price', $this->input->post('it_price'));
				$this->db->set('conit_remark', $this->input->post('it_remark'));
				$this->db->insert('consignment_item');
			}
		}
		parent :: save ($idx);
		return TRUE;
	}

	/**
	 * Get PO Code
	 *
	 * @access	public
	 * @return	integer
	 */
	public function get_current_con_code ()
	{
		$this->db->select('MAX(pi_no)');
		$max_record = $this->one($this->tableName);
		$INIT = substr ($max_record, 0, 2);
		$ID = substr ($max_record, 2, 4);
		if ($INIT == 'IP')
		{
			return $INIT.str_pad($ID+1, 4, "0", STR_PAD_LEFT);
		}
		else
		{
			return 'IP0001';	
		}
	}
}