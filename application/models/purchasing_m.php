<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Purchasing_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'purchasing';
		$this->idx	 = 'po_code';
		$this->fields	 = array(
			'po_code' => array('Po No', TRUE),
			'po_date' => array('PO Date', TRUE),
			'sp_idx' => array('Supplier Idx', TRUE),
			'po_sp_name' => array('Supplier Name', TRUE),
			'po_discount' => array('Discount', TRUE),
			'po_vat' => array('Vat', TRUE),
			'po_sign_by' => array('Sign By', TRUE),
			'po_last_updated_time' => array('Last updated', FALSE),
			'po_last_updated_by' => array('Last updated by', FALSE),
			'po_remark' => array('Remark', FALSE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function save ($code = FALSE)
	{
		
		if ($this->input->post('it_code'))
		{
			foreach ($this->input->post('it_code') as $key => $val)
			{
				$this->db->set('it_code', $this->input->post('it_code'));
				$this->db->set('po_code', $code);
				$this->db->set('poit_qty', $this->input->post('poit_qty'));
				$this->db->set('poit_price', $this->input->post('poit_price'));
				$this->db->set('poit_remark', $this->input->post('poit_remark'));
				$this->db->insert('purchasing_item');
			}
		}
		$this->db->set('po_code','SL120000001');
		parent :: save ($idx);
		return TRUE;
	}

	/**
	 * Get PO Code
	 *
	 * @access	public
	 * @return	integer
	 */
	public function get_current_po_code ()
	{
		$this->db->select('MAX(SUBSTR(pi_no,5))');
		$this->db->where(SUBSTR(po_code,1,4), 'SL'.date('y'));
		$max_record = $this->one($this->tableName);
		$INIT = substr ($max_record, 0, 4);
		$ID = substr ($max_record, 4, 7);
		if ($INIT == 'SL'.date('y'))
		{
			return $INIT.str_pad($ID+1, 7, "0", STR_PAD_LEFT);
		}
		else
		{
			return 'SL'.date('y').'0000001';	
		}
	}
}