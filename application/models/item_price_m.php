<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Employee Model
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * Employee Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Model
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Item_price_m extends MY_Model {
	
	public function __construct ()
	{
		parent :: __construct ();
		$this->tableName = 'item_price';
		$this->idx	 = 'ip_idx';
		$this->fields	 = array(
			'it_code' => array('Code Item', TRUE),
			'ip_date_from' => array('Date from', TRUE),
			'ip_date_to' => array('Date to', FALSE),
			'ip_price' => array('Price', TRUE)
		);
	}

	/**
	 * Save method
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	 */
	public function update ($idx = FALSE, $data = FALSE)
	{
//var_dump($data);exit;
		$this->db->where('ip_idx', $idx);
		$this->db->update('item_price', $data); 
		return TRUE;
	}

	/**
	 * Get record price with filtering it_code
	 *
	 * @access	public
	 * @param	integer
	 * @return	boolean
	*/
	public function get_price ($it_code = FALSE)
	{
		$this->db->where('it_code', $it_code);
		$this->db->order_by('ip_date_from DESC');
		return parent :: get ();
	}
}