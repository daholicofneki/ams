<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Stock/Item Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Item extends MY_Controller {

	public $module = array('stock/item', 'stock', 'item');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		$this->load->model(array(
			'category_m',
			'item_m',
			'item_category_m',
			'item_price_m',
		));
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->params['data'] = $this->item_m->get();
		$this->_view('main_1_3', 'item');
	}

	/**
	 * Add new item
	 *
	 * @access	public
	 * @returnci	parent class function
	 */
	public function insert ()
	{
		if ($this->input->post('save'))
		{
			if ($this->item_m->isValid())
			{
				if ($this->item_m->save())
				{
					setSucces('Data is saved');
					redirect ($this->module[0] . '/update/detail/' . $this->input->post('it_code'));
				}
				else
				{
					setError('Unable to save');
				}
			}
		}
		$this->params['cat'] = $this->category_m->get_category_name();
		$this->_view('main_1_3', 'item_new');
	}

	/**
	 * Update item
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function update ($action, $it_code)
	{
		if ($it_code AND $this->item_m->get($it_code))
		{
			if ($action == 'detail')
			{
				if ($this->input->post('save'))
				{
					if ($this->item_m->isValid())
					{
						if ($this->item_m->save($it_code))
						{
							setSucces('Data is saved');
							redirect ($this->module[0] . '/update/detail/' . $this->input->post('it_code'));
						}
						else
						{
							setError('Unable to save');
						}
					}
				}
			}
			else if ($action == 'category')
			{
				if ($this->input->post('save'))
				{
					$this->item_m->insert_category($it_code);
					redirect ($this->module[0] . '/update/category/' . $it_code);
				}
				$itcat_idx = $this->item_category_m->get_category_name($it_code);
				$this->params['cat'] = $this->category_m->get_category_name(FALSE, TRUE, $itcat_idx);
				$this->params['cat_item'] = $itcat_idx;
			}
			else if ($action == 'price')
			{
				if ($this->input->post('save'))
				{
					$this->item_m->insert_price($it_code);
					redirect ($this->module[0] . '/update/price/' . $it_code);
				}
				else if ($this->input->post('update'))
				{
					$data = array('ip_price'=>$this->input->post('ip_price'));
					if ($this->item_price_m->update($this->input->post('ip_last_idx'), $data))
					{
						setSucces('Data is saved');
						redirect ($this->module[0] . '/update/price/' . $it_code);
					}					
				}
				$this->params['price'] = $this->item_price_m->get_price($it_code);
			}
			$this->params['data'] = $this->item_m->get($it_code);
			$this->params['action'] = $action;
			$this->_view('main_1_3', 'item_detail');
		}
		else
		{
			redirect ($this->module[0]);
		}
		
	}

	/**
	 * Delete item
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function delete ($it_code)
	{
		$this->item_m->delete($it_code);
		redirect ($this->module[0]);
	}

	/**
	 * Delete category item 
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function delete_category_item ($it_code = FALSE, $itcat_idx = FALSE)
	{
		$this->item_category_m->delete($itcat_idx);
		redirect ($this->module[0] . '/update/category/' . $it_code);
	}
}
/* End class Item */
/* Location ./application/controllers/stock/item.php */