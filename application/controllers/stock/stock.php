<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Stock/Stock Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Stock extends MY_Controller {

	public $module = array('stock/stock', 'stock', 'stock');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		/*$this->load->model(array(
			'employee_job_m'
		));*/
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->_view('main_1_3', 'stock');
	}

	/**
	 * List Stock with Expired Date
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function expired ()
	{
		$this->_view('main_1_3', 'stock_expired');
	}

	/**
	 * List Stock with Expired Date
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function buffer ()
	{
		$this->_view('main_1_3', 'buffer');
	}
}
/* End class Stock */
/* Location ./application/controllers/stock/stock.php */