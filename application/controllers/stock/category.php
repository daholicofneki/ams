<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Stock/Category Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Category extends MY_Controller {

	public $module = array('stock/category', 'stock', 'category');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		$this->load->model(array(
			'category_m'
		));
	}

	/**
	 * Category item
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ($type = 'medicine')
	{
		$this->params['data']		= $this->category_m->get_category_name($type);
		$this->params['type']		= $type;
		$this->params['labels']		= $this->category_m->getLabels();
		$this->_view('main_1_3', 'cat');
	}

	/**
	 * Add new category
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert ()
	{
		if ($this->input->post('save'))
		{
			if ($this->category_m->isValid())
			{
				if ($this->category_m->save())
				{
					setSucces('Data is saved');
					redirect ($this->module[0]);
				}
				else
				{
					setError('Unable to save');
				}
			}
		}
		$this->_view('main_1_3', 'cat_new');
	}

	/**
	 * Update category
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function update ($idx)
	{
		if ($idx AND $this->category_m->get($idx))
		{
			if ($this->input->post('save'))
			{
				if ($this->category_m->isValid())
				{
					// save data
					if ($this->category_m->save($idx))
					{
						setSucces('Data is edited');
					}
					else
					{
						setError('Unable to save');
					}
				}
			}
			$this->params['data']		= $this->category_m->get($idx);
			$this->params['labels']		= $this->category_m->getLabels();
			$this->_view('main_1_3', 'cat_new');
		}
		
	}

	/**
	 * Delete category
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function delete ($idx)
	{
		$this->category_m->delete($idx);
		redirect ($this->module[0]);
	}
}
/* End class Item */
/* Location ./application/controllers/stock/category.php */