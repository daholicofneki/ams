<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sales/Customer Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Customer extends MY_Controller {

	public $module = array('sales/customer', 'sales', 'customer');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		$this->load->model(array(
			'customer_m',
		));
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->params['data'] = $this->customer_m->get();
		$this->_view('main_1_3', 'customer');
	}
	
	/**
	 * Add new customer
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert ()
	{
		if ($this->input->post('save'))
		{

#			if ($this->customer_m->isValid())
#			{
				if ($this->customer_m->save())
				{
					setSucces('Data is saved');
					redirect ($this->module[0]);
				}
				else
				{
					setError('Unable to save');
				}
#			}
		}
		$this->_view('main_1_3', 'customer_new');
	}

	/**
	 * Update customer
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function update ($idx)
	{
		if ($idx AND $this->customer_m->get($idx))
		{
			if ($this->input->post('save'))
			{
				if ($this->customer_m->isValid())
				{
					// save data
					if ($this->customer_m->save($idx))
					{
						setSucces('Data is edited');
					}
					else
					{
						setError('Unable to save');
					}
				}
			}
			$this->params['data']		= $this->customer_m->get($idx);
			$this->params['labels']		= $this->customer_m->getLabels();
			$this->_view('main_1_3', 'customer_new');
		}
		
	}

	/**
	 * Delete customer
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function delete ($idx)
	{
		$this->customer_m->delete($idx);
		redirect ($this->module[0]);
	}
}
/* End class Customer */
/* Location ./application/controllers/purchasing/customer.php */