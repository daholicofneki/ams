<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sales/Summary Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Summary extends MY_Controller {

	public $module = array('sales/summary', 'sales', 'summary');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		/*$this->load->model(array(
			'employee_job_m'
		));*/
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->_view('main_1_3', 'summary');
	}

	/**
	 * Daily summary by order
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function order ()
	{
		$this->_view('main_1_3', 'daily_order');
	}

	/**
	 * Daily summary by item
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function item ()
	{
		$this->_view('main_1_3', 'daily_item');
	}

	/**
	 * Daily summary by order
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function monthly ()
	{
		$this->_view('main_1_3', 'monthly');
	}
}
/* End class Summary */
/* Location ./application/controllers/sales/summary.php */