<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sales/Sales Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Sales extends MY_Controller {

	public $module = array('sales/sales', 'sales', 'sales');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		/*$this->load->model(array(
			'employee_job_m'
		));*/
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->_view('main_1_3', 'sales');
	}
	
	/**
	 * Add new sales
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert ()
	{
		$this->_view('main_1_3', 'sales_new');
	}

	/**
	 * Add new return sales 
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert_return ()
	{
		$this->_view('main_1_3', 'return_new');
	}
}
/* End class Sales */
/* Location ./application/controllers/sales/sales.php */