<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Consignment/Consignment Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Consignment extends MY_Controller {

	public $module = array('consignment/consignment', 'consignment', 'consignment');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		$this->load->model(array(
			'consignment_m',
			'consignment_return_m',
		));
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		#$this->params['data'] = $this->employee_job_m->get();
		$this->_view('main_1_3', 'consignment');
	}

	/**
	 * Add new consignment
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert ()
	{
		$this->_view('main_1_3', 'consignment_new');
	}

	/**
	 * Add new return consignment
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert_return ()
	{
		$this->_view('main_1_3', 'return_new');
	}
}
/* End class Consignment */
/* Location ./application/controllers/consignment/consignment.php */