<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Purchasing/Purchasing Controller
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author              NDI-SOFTWARE DEVELOPMENT TEAM
 * @author              Purwandi <free6300@gmail.com>
 * @since		CodeIgniter Version 2.0
 * @filesource
 */

/**
 * MY Controller Class
 *
 * Loads Table
 *
 * @package		CodeIgniter
 * @subpackage	        Controller
 * @author              Purwandi <free6300@gmail.com>
 * @category	        Model
 */

class Purchasing extends MY_Controller {

	public $module = array('purchasing/purchasing', 'purchasing', 'purchasing');

	/**
	 * Constructor
	 *
	 */
	public function __construct ()
	{
		parent :: __construct ();

		$this->load->model(array(
			'purchasing_m'
		));
	}

	/**
	 * Index
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function index ()
	{
		$this->_view('main_1_3', 'purchasing');
	}
	
	/**
	 * Add new purchasing
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function insert ()
	{
		$this->_view('main_1_3', 'purchasing_new');
	}


	/**
	 * Autocomplete using ajax calling data Supplier
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function supplier_ajax ()
	{
		if ($this->input->is_ajax_request())
		{
			$this->load->model('supplier_m');
			$this->db->like('sp_name', $this->input->get('q'));
			echo json_encode($this->supplier_m->get());
		}
	}

	/**
	 * Autocomplete using ajax calling data Item
	 *
	 * @access	public
	 * @return	parent class function
	 */
	public function item_ajax ()
	{
		if ($this->input->is_ajax_request())
		{
			$this->load->model('item_m');
			$this->db->join('item_price', 'item_price.it_code = item.it_code');
			$this->db->like('it_name', $this->input->get('q'));
			$this->db->where('ip_valid', '1');
			echo json_encode($this->item_m->get());
		}
	}

	
}
/* End class Purchasing */
/* Location ./application/controllers/purchasing/purchasing.php */