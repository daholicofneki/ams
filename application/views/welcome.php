<!DOCTYPE html>
<!-- Created by : Neki Arismi neki.arismi@gmail.com 0856 874 5318  -->
<html>
<head>
	<meta charset="UTF-8" />
	<title>Home</title>
	<link href="<?php echo base_url()?>static/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>static/css/bootstrap-responsive.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>static/css/custom.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="container">

      <div class="hero-unit">
        <h1>Pan Rita</h1>
	<p>Apotik Management Software</p>
      </div>

      <div class="row">

	<div class="span12">
	      <ul class="thumbnails">
		<li class="span3">
		  <div class="thumbnail">
		    <div class="caption">
		      <h3>STOCK</h3>
		      <p>
			    <ul class="nav nav-tabs nav-stacked">
			      <li><a href="<?php echo base_url().'stock/item'?>">Item</a></li>
			      <li><a href="<?php echo base_url().'stock/price'?>">Price List</a></li>
			      <li><a href="<?php echo base_url().'stock/stock'?>">Stock</a></li>
			    </ul>
		      </p>
		    </div>
		  </div>
		</li>
		<li class="span3">
		  <div class="thumbnail">
		    <div class="caption">
		      <h3>PURCHASING</h3>
		      <p>
			    <ul class="nav nav-tabs nav-stacked">
			      <li><a href="<?php echo base_url().'purchasing/supplier'?>">Supplier Data</a></li>
			      <li><a href="<?php echo base_url().'purchasing/purchasing'?>">Purchasing</a></li>
			    </ul>
		      </p>
		    </div>
		  </div>
		</li>
		<li class="span3">
		  <div class="thumbnail">
		    <div class="caption">
		      <h3>SALES</h3>
		      <p>
			    <ul class="nav nav-tabs nav-stacked">
			      <li><a href="<?php echo base_url().'sales/sales/insert'?>">Add New Sales</a></li>
			      <li><a href="<?php echo base_url().'sales/summary/order'?>">Summary by Order, Item, Monthly</a></li>
			    </ul>
		      </p>
		    </div>
		  </div>
		</li>
		<li class="span3">
		  <div class="thumbnail">
		    <div class="caption">
		      <h3>CONSIGNMENT</h3>
		      <p>
			    <ul class="nav nav-tabs nav-stacked">
			      <li><a href="<?php echo base_url().'consignment/consignment/insert'?>">Add New Consignment</a></li>
			      <li><a href="<?php echo base_url().'consignment/consignment/insert_return'?>">Add New Return</a></li>
			      <li><a href="<?php echo base_url().'consignment/summary/order'?>">Summary by Order, Item, Monthly</a></li>
			      <li><a href="<?php echo base_url().'consignment/supplier'?>">Supplier</a></li>
			      <li><a href="<?php echo base_url().'consignment/stock'?>">Stock</a></li>
			    </ul>
		      </p>
		    </div>
		  </div>
		</li>
	      </ul>	
	</div>
	
        <div class="span12">
	  <form class="well form-inline">
	    <input type="text" class="input-big" placeholder="Input your usename">
	    <input type="password" class="input-small" placeholder="Password">
	    <button type="submit" class="btn btn-primary">Log In</button>
	  </form><hr>
        </div>

        <div class="offset8 span4">
		<address>
		    <strong>APOTEK "PAN RITA"</strong><br>
		    Jl. Sungai Bambu Raya No. 10 Kel. Kebon Bawang, Tj. Priok<br>
		    Jakarta Utara<br>
		    <abbr title="Phone">Telepon: </abbr> (021) 9541 2158
		</address>
	</div>	

      </div>

    </div>
</body>
</html>