<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formid").validate();
    });
</script>
<div class="page-header">
    <h2>Add New Customer</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <?php echo form_text('Customer Name','cus_name',(isset($data->cus_name))?$data->cus_name:'','class="span5" maxlength="64"');?>
    <?php echo form_text('Contact Person','cus_pic',(isset($data->cus_pic))?$data->cus_pic:'','class="span3" maxlength="32"');?>
    <?php echo form_text('Phone','cus_phone',(isset($data->cus_phone))?$data->cus_phone:'','class="span3" maxlength="64"');?>
    <?php echo form_text('Handphone','cus_handphone',(isset($data->cus_handphone))?$data->cus_handphone:'','class="span3" maxlength="64"');?>
    <?php echo form_text('Fax','cus_fax',(isset($data->cus_fax))?$data->cus_fax:'','class="span3" maxlength="32"');?>
    <?php echo form_text('Email','cus_email',(isset($data->cus_email))?$data->cus_email:'','class="span3" maxlength="64"');?>
    <?php echo form_area('Address','cus_address',(isset($data->cus_address))?$data->cus_address:'','class="span5"');?>
    <?php echo form_text('City','cus_city',(isset($data->cus_city))?$data->cus_city:'','class="span3" maxlength="32"');?>
    <?php echo form_area('Remark','cus_remark',(isset($data->cus_remark))?$data->cus_remark:'','class="span5"');?>

    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->cus_idx)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete/'.$data->cus_idx,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>