<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
	$("#sl_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		showAnim: 'fold'
	});
        $("#formid").validate();

	$('#sl_sp_name').autocomplete('<?php echo base_url()?>purchasing/purchasing/supplier_ajax',{
		parse: function(data){
		    var parsed = [];
		    for (var i=0; i < data.length; i++) {
			parsed[i] = { data: data[i], value: data[i].id_rekening };
		    }
		    return parsed;
		},
		formatItem: function(data,i,max){
		    return  '<div class="search_content"><bold>[ '+data.id_rekening+' ]</bold> &nbsp;<small>'+data.nama_rekening+'</small></div>';
		},
		width: 600, 
		dataType: 'json' 
	    }).result( 
		    function(event,data,formated){
	    		$('#sub_rekening').val(data.id_rekening);
			$('#sub_keterangan').val(data.nama_rekening +' <?php echo date("d M Y");?>');
    		}
	);

	$('#it_show').autocomplete('<?php echo base_url()?>purchasing/purchasing/item_ajax',{
		parse: function(data){
		    var parsed = [];
		    for (var i=0; i < data.length; i++) {
			parsed[i] = { data: data[i], value: data[i].id_rekening };
		    }
		    return parsed;
		},
		formatItem: function(data,i,max){
		    return  '<div class="search_content"><bold>[ '+data.id_rekening+' ]</bold> &nbsp;<small>'+data.nama_rekening+'</small></div>';
		},
		width: 600, 
		dataType: 'json' 
	    }).result( 
		    function(event,data,formated){
	    		$('#sub_rekening').val(data.id_rekening);
			$('#sub_keterangan').val(data.nama_rekening +' <?php echo date("d M Y");?>');
    		}
	);

    });
</script>
<div class="page-header">
    <h2>Add New Sales</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <legend>Sales Detail</legend>
    <?php echo form_text('Sales Date','sl_date','','class="span2" placeholder="yyyy-mm-dd" id="sl_date" class="datepicker"');?>
    <?php echo form_hidden('cus_idx','');?>
    <?php echo form_text('Customer Name','sl_cus_name','','id="sl_cus_name" class="span5" maxlength="64"');?>

    <legend>Item List</legend>
    <div class="well form-inline">
    <?php echo form_hidden('it_code','');?>
    <?php echo form_hidden('it_name','');?>
    <input type="text" name="it_show" placeholder="Item Name" id="it_show" class="span3">
    <input type="text" name="it_desc" placeholder="Description" class="span2">
    <input type="text" name="it_price" placeholder="Price" class="span2">
    <input type="text" name="it_qty" placeholder="Qty" class="span1">
    <input type="text" name="it_remark" placeholder="Remark" class="span2" maxlength="32">
    <button type="submit" class="btn"><span class="icon-plus"></span></button>
    </div>
    <table class="table table-bordered">
      <thead>
	<tr>
		<th style="width:30%">Model</th>
		<th style="width:25%">Desc</th>
		<th style="width:10%">Price</th>
		<th style="width:7%">Qty</th>
		<th style="width:12%">Total</th>
		<th>Remark</th>
	</tr>
      </thead>
      <tbody></tbody>
      <tfoot>
	<tr>
		<th colspan="3" style="text-align: right">Sub Total</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">Discount</th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">Before VAT</th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">VAT</th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">Grand Total</th>
		<th></th>
		<th></th>
	</tr>
      </tfoot>
    </table>

    <legend>Sales Additional</legend>
    <?php echo form_text('Sign By','sl_sign_by','','class="span2" maxlength="32"');?>
    <?php echo form_area('Remark','sl_remark','','class="span8"');?>
    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->sl_code)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete/'.$data->sl_code,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>