<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
	$("#po_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		showAnim: 'fold'
	});
        $("#formid").validate();

	$('#po_sp_name').autocomplete('<?php echo base_url()?>purchasing/purchasing/supplier_ajax',{
		parse: function(data){
		    var parsed = [];
		    for (var i=0; i < data.length; i++) {
			parsed[i] = { data: data[i], value: data[i].sp_name };
		    }
		    return parsed;
		},
		formatItem: function(data,i,max){
		    return  '<div class="search_content"><bold> '+data.sp_name+' </bold> &nbsp;<small>'+data.sp_pic+'</small></div>';
		},
		width: 450, 
		dataType: 'json' 
	    }).result( 
		    function(event,data,formated){
	    		$('#po_sp_name').val(data.sp_name);
			$('#sp_idx').val(data.sp_idx);
    		}
	);

	$('#it_show').autocomplete('<?php echo base_url()?>purchasing/purchasing/item_ajax',{
		parse: function(data){
		    var parsed = [];
		    for (var i=0; i < data.length; i++) {
			parsed[i] = { data: data[i], value: data[i].it_name };
		    }
		    return parsed;
		},
		formatItem: function(data,i,max){
		    return  '<div class="search_content"><bold> '+data.it_code+' </bold> &nbsp;<small>'+data.it_name+'</small> <br /><small>'+data.it_desc+'</small></div>';
		},
		width: 250, 
		dataType: 'json' 
	    }).result( 
		    function(event,data,formated){
			$('#it_code').val(data.it_code);
			$('#it_name').val(data.it_name);
			$('#it_desc').val(data.it_desc);
			$('#it_price').val(data.ip_price);
	    		$('#it_show').val(data.it_code +' '+data.it_name);
    		}
	);
    });

    var id = 0;
    function insert_item ()
    {
	if ($("#it_show").val() == '') {alert ('Please fill item first'); $("#it_show").focus()}
	else if ($("#it_qty").val() == '') {alert ('Please fill qty'); $("#it_qty").focus()}
	else if ($("#it_price").val() == '') {alert ('Please fill price'); $("#it_price").focus()}
	else insert_item_print ();
    }

    function insert_item_print ()
    {
	$tot = removecomma($("#it_price").val()) * removecomma($("#it_qty").val());
	$string = '<tr id="item_'+id+'">'+
		    '<td>'+$("#it_show").val()+'<input type="hidden" name="item_code[]" value="'+$("#it_code").val()+'"><input type="hidden" name="item_name[]" value="'+$("#it_name").val()+'"></td>'+
		    '<td>'+$("#it_desc").val()+'<input type="hidden" name="item_desc[]" value="'+$("#it_desc").val()+'"></td>'+
		    '<td>'+addcomma($("#it_price").val())+'<input type="hidden" name="item_price[]"  value="'+removecomma($("#it_price").val())+'"></td>'+
		    '<td>'+addcomma($("#it_qty").val())+'<input type="hidden" name="item_qty[]" value="'+removecomma($("#it_qty").val())+'"></td>'+
		    '<td>'+addcomma($tot)+'<input type="hidden" name="item_total[]" value="'+$tot+'"></td>'+
		    '<td>'+$("#it_remark").val()+'<input type="hidden" name="item_remark[]" value="'+$("#it_remark").val()+'"></td>'+
		    '<td><a href="" onclick="edit_item(\''+id+
						    '\', \''+$("#it_show").val()+
						    '\', \''+$("#it_code").val()+
						    '\', \''+$("#it_name").val()+
						    '\', \''+$("#it_desc").val()+
						    '\', \''+$("#it_price").val()+
						    '\', \''+$("#it_qty").val()+
						    '\', \''+$("#it_remark").val()+
						    '\'); return false">E</a> | <a href="" onclick="remove_item(\''+id+'\'); return false;">D</a></td>'+
		   '</tr>';
	if ( $("#mode").val() == 'insert')
	{
	    $('#item').append( $string );
	}
	else {
	    $('tr#item_'+$("#id_item").val()).replaceWith($string);
	}
	id++;
	set_item ('');
	update_amount();
    }

    function set_item ( VAL )
    {
	$("#it_show").val( VAL );
	$("#it_code").val( VAL );
	$("#it_name").val( VAL );
	$("#it_desc").val( VAL );
	$("#it_price").val( VAL );
	$("#it_qty").val( VAL );
	$("#it_remark").val( VAL );
	$("#mode").val( 'insert' );
    }

    function edit_item ( ID, SHOW, CODE, NAME, DESC, PRICE, QTY, REMARK)
    {
	$("#id_item").val( ID );
	$("#it_code").val( CODE );
	$("#it_name").val( NAME );
	$('#it_show').val(CODE +' '+NAME);
	$("#it_desc").val( DESC );
	$("#it_price").val( PRICE );
	$("#it_qty").val( QTY );
	$("#it_remark").val( REMARK );
	$("#mode").val('edit');
	return false;
    }

    function remove_item ( VAL )
    {
	$('#item_'+VAL).remove();
	return false;
    }

    function update_amount()
    {
	var rows = $("#item_list >tbody tr");
	var val = new Array(0,0,0,0);	// Qty, Total, Disc, Grand Total

	rows.children("td:nth-child(4)").each(function() {
	    if ($(this).text() != '') 
		val[0] += parseFloat(removecomma($(this).text()));
	});

	rows.children("td:nth-child(5)").each(function() {
	    if ($(this).text() != '') 
		val[1] += parseFloat(removecomma($(this).text()));
	});

	val[2] = removecomma($("#tot_disc").val());
	val[3] = val[1]-val[2];

	$("#tot_qty").val(addcomma(val[0]));
	$("#tot_amount").val(addcomma(val[1]));
	$("#tot_grand").val(addcomma(val[3]));
    }
</script>
<div class="page-header">
    <h2>Add New Purchasing</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <legend>PO Detail</legend>
    <?php echo form_text('PO Date *','po_date','','class="span2" placeholder="yyyy-mm-dd" id="po_date" class="datepicker"');?>
    <?php echo form_hidden('sp_idx','','id="sp_idx"');?>
    <?php echo form_text('Supplier Name *','po_sp_name','','id="po_sp_name" class="span5" maxlength="64"');?>

    <legend>Item List</legend>
    <div class="well form-inline">
    <?php echo form_hidden('it_code','','id="it_code"');?>
    <?php echo form_hidden('it_name','','id="it_name"');?>
    <input type="text" name="it_show" placeholder="Item Name" id="it_show" class="span3">
    <input type="text" name="it_desc" placeholder="Description" id="it_desc" class="span2">
    <input type="text" name="it_price" placeholder="Price" id="it_price" class="span2" onkeyup="formatNumber(this, 'comma')">
    <input type="text" name="it_qty" placeholder="Qty" id="it_qty" class="span1" onkeyup="formatNumber(this, 'comma')">
    <input type="text" name="it_remark" placeholder="Remark" id="it_remark" class="span2" maxlength="32">
    <input type="hidden" value="insert" name="mode" id="mode">
    <input type="hidden" value="" name="id_item" id="id_item">
    <?php echo form_button('insert-item','Insert','onclick="return insert_item()" class="btn"')?>
    </div>
    <table class="table table-bordered" id="item_list">
      <thead>
	<tr>
		<th style="width:30%">Model</th>
		<th style="width:25%">Desc</th>
		<th style="width:10%">Price</th>
		<th style="width:7%">Qty</th>
		<th style="width:12%">Total</th>
		<th colspan="2">Remark</th>
	</tr>
      </thead>
      <tbody id="item">
      </tbody>
      <tfoot>
	<tr>
		<th colspan="3" style="text-align: right">Sub Total</th>
		<th><input type="text" id="tot_qty" style="width:100%"></th>
		<th><input type="text" id="tot_amount" style="width:100%"></th>
		<th colspan="2"></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">Discount</th>
		<th><input type="text" id="tot_disc" value="0" style="width:100%" onblur="update_amount()" onkeyup="formatNumber(this, 'comma')"></th>
		<th colspan="2"></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align: right">Grand Total</th>
		<th><input type="text" id="tot_grand" style="width:100%"></th>
		<th colspan="2"></th>
	</tr>
      </tfoot>
    </table>

    <legend>PO Additional</legend>
    <?php echo form_text('Sign By *','po_sign_by','','class="span2" maxlength="32"');?>
    <?php echo form_area('Remark','po_remark','','class="span8"');?>
    <p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->sp_idx)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete_supplier/'.$data->sp_idx,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>