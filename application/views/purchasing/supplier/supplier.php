<div class="page-header">
    <h2>List Supplier</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <th>#</th>
      <th>Full Name</th>
      <th>Phone</th>
      <th>Fax</th>
      <th>Email</th>
    </tr>
  </thead>
  <tbody>
<?php if ($data):?>
<?php $i = 1; ?>
<?php foreach ($data as $item):?>
    <tr>
      <th rowspan="2"><?php echo $i++ ?></th>
      <th><?php echo anchor($module[0].'/update/'.$item->sp_idx, $item->sp_name)?></th>
      <th><?php echo $item->sp_phone ." ". $item->sp_handphone?></th>
      <th><?php echo $item->sp_fax?></th>
      <th><?php echo $item->sp_email?></th>
    </tr>
    <tr>
      <th colspan="4"><?php echo $item->sp_address?></th>
    </tr>
<?php endforeach;?>
<?php else:?>
    <tr>
      <th colspan="5">There is no data. <?php echo anchor($module[0].'/insert','Please input one here')?></th>
    </tr>
<?php endif;?>
  </tbody>
</table>