<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formid").validate();
    });
</script>
<div class="page-header">
    <h2>Add New Supplier</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <?php echo form_text('Supplier Name *','sp_name',(isset($data->sp_name))?$data->sp_name:'','class="span5" maxlength="64"');?>
    <?php echo form_text('Contact Person','sp_pic',(isset($data->sp_pic))?$data->sp_pic:'','class="span3" maxlength="32"');?>
    <?php echo form_text('Phone','sp_phone',(isset($data->sp_phone))?$data->sp_phone:'','class="span3" maxlength="64"');?>
    <?php echo form_text('Handphone','sp_handphone',(isset($data->sp_handphone))?$data->sp_handphone:'','class="span3" maxlength="64"');?>
    <?php echo form_text('Fax','sp_fax',(isset($data->sp_fax))?$data->sp_fax:'','class="span3" maxlength="32"');?>
    <?php echo form_text('Email','sp_email',(isset($data->sp_email))?$data->sp_email:'','class="span3" maxlength="64"');?>
    <?php echo form_area('Address *','sp_address',(isset($data->sp_address))?$data->sp_address:'','class="span5"');?>
    <?php echo form_text('City *','sp_city',(isset($data->sp_city))?$data->sp_city:'','class="span3" maxlength="32"');?>
    <?php echo form_area('Remark','sp_remark',(isset($data->sp_remark))?$data->sp_remark:'','class="span5"');?>

    <p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->sp_idx)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete/'.$data->sp_idx,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>