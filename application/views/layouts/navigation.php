<?php
$nav = array (
	'stock'		=> array (
				  'Item'=>array(
					'Add New::item/insert::',
					'List::item/::item/update',
				  ),
				  'Item Detail'=>array(
					'Category::category/::category/insert::category/update::category/index::',
					'Price::price/::'
				  ),
				  'Stock'=>array(
					'List::stock/::stock/stock::',
					'List ED::stock/expired',
					'Buffer Stock::stock/buffer',
				  )),
	'purchasing'	=> array (
				  'Supplier'=>array(
					'Add New::supplier/insert::',
					'List::supplier/::supplier/update::'
				  ),
				  'Purchasing'=>array(
					'Add new::purchasing/insert::',
					'List::purchasing/::'
				  )),
	'sales'		=> array (
				  'Sales'=>array(
					'Add New::sales/insert::',
					'Add New Return::sales/insert_return::',
				  ),
				  'Customer'=>array(
					'Add New::customer/insert::',
					'List::customer/::customer/update::'
				  ),
				  'Summary'=>array(
					'Daily by Order::summary/order::',
					'Daily by Item::summary/item::',
					'Monthly::summary/monthly::',
				  )),
	'consignment'	=> array (
				  'Consignment'=>array(
					'Add New::consignment/insert::',
					'Add New Return::consignment/insert_return::',
				  ),
				  'Summary'=>array(
					'Daily by Order::summary/order::',
					'Daily by Item::summary/item::',
					'Monthly::summary/monthly::',
				  ),
				  'Supplier'=>array(
					'Add New::supplier/insert::',
					'List::supplier/::supplier/update::'
				  ),
				  'Stock'=>array(
					'List::stock/::'
				  ))
	);