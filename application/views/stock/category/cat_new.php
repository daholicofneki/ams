<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formid").validate();
    });
</script>
<div class="page-header">
    <h2>Add New Category</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <?php echo form_drop('Type *','cat_type',array('medicine'=>'Medicine','healthcare-tools'=>'Healthcare Tools','consumer-goods'=>'Consumer Goods','others'=>'Others'),(isset($data->cat_type))?$data->cat_type:'');?>
    <?php echo form_text('Category *','cat_name',(isset($data->cat_name))?$data->cat_name:'','class="span5"');?>
    <p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->cat_idx)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete/'.$data->cat_idx,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>