<div class="page-header">
    <h2>Current Available Category</h2>
</div>
<div class="tabbable" style="margin-bottom: 9px;">
    <ul class="nav nav-tabs">
	<li<?php echo ($type=='medicine')?' class="active"':'' ?>><?php echo anchor($module[0].'/index/medicine','Medicine')?></li>
	<li<?php echo ($type=='healthcare-tools')?' class="active"':'' ?>><?php echo anchor($module[0].'/index/healthcare-tools','Healthcare Tools')?></li>
	<li<?php echo ($type=='medical-tools')?' class="active"':'' ?>><?php echo anchor($module[0].'/index/medical-tools','Medical Tools')?></li>
	<li<?php echo ($type=='consumer-goods')?' class="active"':'' ?>><?php echo anchor($module[0].'/index/consumer-goods','Consumer Goods')?></li>
	<li<?php echo ($type=='others')?' class="active"':'' ?>><?php echo anchor($module[0].'/index/others','Others')?></li>
    </ul>
    <div class="tab-content">
	<?php if ($data):?>
	<?php foreach ($data as $item):?>
	<?php echo anchor($module[0].'/update/'.$item->cat_idx, $item->cat_name, 'class="btn"')?> &nbsp; 
	<?php endforeach;?>
	<?php else:?>
	There is no data. <?php echo anchor($module[0].'/insert','Please input one here')?>
	<?php endif;?>
    </div>
</div><hr>
<?php echo anchor($module[0].'/insert','Add new category', 'class="btn btn-primary"')?>