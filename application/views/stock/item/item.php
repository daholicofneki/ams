<div class="page-header">
    <h2>List Item</h2>
</div>
<table class="table">
  <thead>
    <tr>
      <th width="10%">Code</th>
      <th width="40%">Name</th>
      <th>Desc</th>
    </tr>
  </thead>
  <tbody>
  <?php if ($data):?>
    <?php foreach ($data as $item):?>
    <tr>
      <td><?php echo $item->it_code ?></td>
      <td><?php echo anchor($this->module[0] . '/update/detail/' . $item->it_code, $item->it_name)?></td>
      <td><?php echo $item->it_desc ?></td>
    </tr>
    <?php endforeach;?>
  <?php else:?>
    There is no data. <?php echo anchor($module[0].'/insert_category','Please input one here')?>
  <?php endif;?>
  </tbody>
</table>