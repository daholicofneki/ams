<?php
$category = array();
for($i=0; $i<count($cat); $i++)
{
    $category[$cat[$i]->cat_idx] = $cat[$i]->cat_name;
}
?>
<script src="<?php echo base_url()?>static/js/chosen.jquery.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>static/css/chosen.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formid").validate();
    });
</script>
<div class="page-header">
    <h2>Add New Item</h2>
</div>
<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
    <legend>Item Description</legend>
    <?php echo view_errors();?>
    <?php echo form_text('Code *','it_code','','class="span2" maxlength="8" placeholder="Code item"', 'Please input 8 character. Alphabet numeric only.<br />Don\'t start using \'9\'. It is used for consignment item.');?>
    <?php echo form_text('Name *','it_name','','class="span5" placeholder="Name"');?>
    <?php echo form_text('Description *','it_desc','','class="span5" placeholder="Item Description"', 'Give the best description to describe the item is.');?>
    <?php echo form_text('Qty *','it_qty','1','class="span1"');?>
    <?php echo form_text('Unit *','it_unit','Qty','class="span1"');?>
    <?php echo form_text('Produced by','it_produced_by','','class="span3"');?>
    <div class="control-group">
        <label class="control-label" for="it_has_ed">Has ED</label>
        <div class="controls">
            <input type="checkbox" name="it_has_ed" value="1" id="yes"<?php (isset($data->it_has_ed) && $data->it_has_ed==1)?' checked':'' ?>> Yes
            <p class="help-block">Does the item has Expired Date?</p>
        </div>
    </div>

    <legend>Category</legend>
    <?php echo form_drop('Type *','it_type',array('medicine'=>'Medicine','healthcare-tools'=>'Healthcare Tools','medical-tools'=>'Medical Tools','consumer-goods'=>'Consumer Goods','others'=>'Others'),'');?>
    <div class="control-group">
        <label class="control-label" for="has_ed">Category</label>
        <div class="controls">
            <?php echo form_dropdown ('cats[]',$category, '','id="cats" class="chzn-select" style="width:100%" multiple');?>
            <p class="help-block">Choose category that best describe about this item.</p>
        </div>
    </div>

    <legend>Setup</legend>
    <?php echo form_text('Price *','it_price','0','class="span2"');?>

    <p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
    <div class="form-actions">
        <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
        <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
        <?php if(isset($data->it_code)): ?><div class="pull-right"><?php echo anchor($module[0].'/delete_item/'.$data->it_code,'Delete', 'class="btn btn-danger"')?></div><?php endif; ?>
    </div>
<?php echo form_close();?>
<script type="text/javascript">
    $(".chzn-select").chosen();
    $(".chzn-select-deselect").chosen({allow_single_deselect:true});
</script>