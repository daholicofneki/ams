<script src="<?php echo base_url()?>static/js/chosen.jquery.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>static/css/chosen.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url()?>static/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
	<?php if ($action == 'price'): ?>
	$("#ip_date_from").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		showAnim: 'fold'
	});
	<?php endif; ?>

        $("#formid").validate();
    });
</script>
<div class="page-header">
    <h2>Detail Item : <?php echo '['.strtoupper($data->it_code).'] '.$data->it_name ?></h2>
</div>
<div class="tabbable" style="margin-bottom: 9px;">
    <ul class="nav nav-tabs">
	<li<?php echo ($action == 'detail')?' class="active"':'' ?>><?php echo anchor($module[0].'/update/detail/'.$data->it_code,'Detail')?></li>
	<li<?php echo ($action == 'category')?' class="active"':'' ?>><?php echo anchor($module[0].'/update/category/'.$data->it_code,'Category')?></li>
	<li<?php echo ($action == 'price')?' class="active"':'' ?>><?php echo anchor($module[0].'/update/price/'.$data->it_code,'Price')?></li>
    </ul>
    <div class="tab-content">
        <?php if ($action == 'detail'): ?>
        <?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
            <?php echo view_errors();?>
            <?php echo form_hidden('it_code',$data->it_code);?>
            <?php echo form_text('Name *','it_name',$data->it_name,'class="span5" placeholder="Name"');?>
            <?php echo form_text('Description *','it_desc',$data->it_desc,'class="span5" placeholder="Item Description"', 'Give the best description to describe the item is.');?>
	    <?php echo form_drop('Type *','it_type',array('medicine'=>'Medicine','healthcare-tools'=>'Healthcare Tools','medical-tools'=>'Medical Tools','consumer-goods'=>'Consumer Goods','others'=>'Others'),$data->it_type);?>
            <div class="control-group">
                <label class="control-label" for="it_has_ed">Has ED</label>
                <div class="controls">
                    <input type="checkbox" name="it_has_ed" value="1" id="yes"<?php echo ($data->it_has_ed=='1')?' checked':'' ?>> Yes
                    <p class="help-block">Does the item has Expired Date?</p>
                </div>
            </div>
	    <?php echo form_text('Qty *','it_qty',$data->it_qty,'class="span1"');?>
	    <?php echo form_text('Unit *','it_unit',$data->it_unit,'class="span1"');?>
	    <?php echo form_text('Produced by','it_produced_by',$data->it_produced_by,'class="span3"');?>
	    <p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
            <div class="form-actions">
                <?php echo form_submit('save','Update data', 'class="btn btn-primary"')?>
                <?php echo anchor($module[0],'Cancel', 'class="btn"')?>
                <div class="pull-right"><?php echo anchor($module[0].'/delete/'.$data->it_code,'Delete', 'class="btn btn-danger"')?></div>
            </div>
        <?php echo form_close();?>
        <?php elseif ($action == 'category'): ?>
	    <legend>Add new category</legend>
	    <?php
	    for($i=0; $i<count($cat); $i++) $category[$cat[$i]->cat_idx] = $cat[$i]->cat_name;
	    ?>
	    <?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
		<div class="control-group">
		    <label class="control-label" for="has_ed">Category</label>
		    <div class="controls">
			<?php echo form_dropdown ('cats[]',$category, '','id="cats" class="chzn-select" style="width:100%" multiple');?>
			<p class="help-block">Choose category that best describe about this item.</p>
		    </div>
		</div>
		<div class="form-actions">
		    <?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
		</div>
	    <?php echo form_close();?>
	    <script type="text/javascript">
		$(".chzn-select").chosen();
		$(".chzn-select-deselect").chosen({allow_single_deselect:true});
	    </script>
	    <legend>Current category <span class="label label-important">Click related category to delete category from the list</span></legend>
	    <?php if ($cat_item):?>
	    <?php foreach ($cat_item as $item):?>
	    <?php echo anchor($module[0].'/delete_category_item/'.$item->it_code.'/'.$item->itcat_idx, $item->cat_name, 'class="btn"')?> &nbsp; 
	    <?php endforeach;?>
	    <?php else:?>
	    There is no data.
	    <?php endif;?>
	    <hr />

        <?php elseif ($action == 'price'): ?>
	    <table style="width:50%" class="table table-bordered">
	      <thead>
		<tr>
		  <th width="5%">#</th>
		  <th width="25%">From</th>
		  <th width="25%">To</th>
		  <th>Price (Rp)</th>
		</tr>
	      </thead>
	      <tbody>
		<?php if ($price):?>
		<?php $i=1; ?>
		<?php foreach ($price as $item):?>
		<?php if($i==1) { $ip_last_idx=$item->ip_idx; } ?>
		<tr>
		  <td><?php echo $i++?></td>
		  <td><?php echo date('d-M-Y',strtotime($item->ip_date_from)) ?></td>
		  <td><?php echo ($item->ip_date_to!='') ? date('d-M-Y',strtotime($item->ip_date_to)):'' ?></td>
		  <td style="align:right"><?php echo number_format($item->ip_price,0) ?></td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
		    <td colspan="5">There is no data.</td>
		</tr>
		<?php endif;?>
	      </tbody>
	    </table>
	    <div class="span4">
		<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
		    <legend>Update current price</legend>
		    <?php echo form_hidden('ip_last_idx',$ip_last_idx);?>
		    <?php echo form_text('Price *','ip_price','','placeholder="Rp"');?>
		    <div class="form-actions">
			<?php echo form_submit('update','Update', 'class="btn"')?>
		    </div>
		<?php echo form_close();?>
		<p><code>Note: </code> &nbsp; All field mark with <code>*</code> are required.</p>
	    </div>
	    <div class="span5">
		<?php echo form_open(uri_string(),array('id'=>'formid', 'class'=>'form-horizontal'))?>
		    <legend>Add new price</legend>
		    <?php echo form_hidden('ip_last_idx',$ip_last_idx);?>
		    <?php echo form_text('From *','ip_date_from','','placeholder="yyyy-mm-dd" id="ip_date_from" class="datepicker"', 'Price valid from date. Format yyyy-mm-dd');?>
		    <?php echo form_text('Price *','ip_price','','placeholder="Rp"');?>
		    <div class="form-actions">
			<?php echo form_submit('save','Save data', 'class="btn btn-primary"')?>
		    </div>
		<?php echo form_close();?>
	    </div>
        <?php endif; ?>
    </div>
</div>