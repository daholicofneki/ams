/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : apotik

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2012-03-30 09:54:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_category`
-- ----------------------------
DROP TABLE IF EXISTS `tb_category`;
CREATE TABLE `tb_category` (
  `cat_idx` int(11) NOT NULL AUTO_INCREMENT,
  `cat_type` varchar(32) NOT NULL,
  `cat_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`cat_idx`),
  UNIQUE KEY `tb_category_cat_name_idx` (`cat_name`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_category
-- ----------------------------
INSERT INTO `tb_category` VALUES ('3', 'medicine', 'Suplemen');
INSERT INTO `tb_category` VALUES ('5', 'medicine', 'Narkotika & Psikotropika');
INSERT INTO `tb_category` VALUES ('6', 'medicine', 'Sirup');
INSERT INTO `tb_category` VALUES ('7', 'medicine', 'Tablet');
INSERT INTO `tb_category` VALUES ('8', 'medicine', 'Injeksi');
INSERT INTO `tb_category` VALUES ('9', 'medicine', 'Semi Padat');
INSERT INTO `tb_category` VALUES ('10', 'medicine', 'Obat Luar');
INSERT INTO `tb_category` VALUES ('11', 'healthcare-tools', 'Home Healthcare');
INSERT INTO `tb_category` VALUES ('12', 'healthcare-tools', 'Disposable');
INSERT INTO `tb_category` VALUES ('13', 'healthcare-tools', 'Medical Device');
INSERT INTO `tb_category` VALUES ('14', 'consumer-goods', 'Kosmetika');
INSERT INTO `tb_category` VALUES ('15', 'consumer-goods', 'Susu');

-- ----------------------------
-- Table structure for `tb_consignment`
-- ----------------------------
DROP TABLE IF EXISTS `tb_consignment`;
CREATE TABLE `tb_consignment` (
  `con_code` varchar(11) NOT NULL DEFAULT '',
  `con_date` date DEFAULT NULL,
  `sp_idx` int(11) NOT NULL DEFAULT '0',
  `con_sp_name` varchar(64) DEFAULT NULL,
  `con_discount` decimal(12,2) DEFAULT '0.00',
  `con_vat` decimal(3,2) DEFAULT '0.00',
  `con_sent_by` varchar(32) DEFAULT NULL,
  `con_received_by` varchar(32) DEFAULT NULL,
  `con_last_updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `con_last_updated_by` varchar(16) DEFAULT NULL,
  `con_remark` text,
  PRIMARY KEY (`con_code`,`sp_idx`),
  UNIQUE KEY `tb_consignment_con_code_idx` (`con_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_consignment
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_consignment_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_consignment_item`;
CREATE TABLE `tb_consignment_item` (
  `conit_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `con_code` varchar(11) NOT NULL DEFAULT '',
  `conit_qty` decimal(8,2) DEFAULT NULL,
  `conit_price` decimal(12,2) DEFAULT NULL,
  `conit_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`conit_idx`,`it_code`,`con_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_consignment_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_consignment_return`
-- ----------------------------
DROP TABLE IF EXISTS `tb_consignment_return`;
CREATE TABLE `tb_consignment_return` (
  `rcon_code` varchar(11) NOT NULL DEFAULT '',
  `con_code` varchar(11) NOT NULL DEFAULT '',
  `rcon_date` date DEFAULT NULL,
  `sp_idx` int(11) NOT NULL DEFAULT '0',
  `rcon_sp_name` varchar(64) DEFAULT NULL,
  `rcon_discount` decimal(12,2) DEFAULT '0.00',
  `rcon_vat` decimal(3,2) DEFAULT '0.00',
  `rcon_sent_by` varchar(32) DEFAULT NULL,
  `rcon_received_by` varchar(32) DEFAULT NULL,
  `rcon_last_updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `rcon_last_updated_by` varchar(16) DEFAULT NULL,
  `rcon_remark` text,
  PRIMARY KEY (`rcon_code`,`con_code`,`sp_idx`),
  UNIQUE KEY `tb_consignment_con_code_idx` (`con_code`) USING BTREE,
  KEY `tb_consignment_return_sp_idx_fk` (`sp_idx`),
  CONSTRAINT `tb_consignment_return_con_code_fk` FOREIGN KEY (`con_code`) REFERENCES `tb_consignment` (`con_code`) ON UPDATE CASCADE,
  CONSTRAINT `tb_consignment_return_sp_idx_fk` FOREIGN KEY (`sp_idx`) REFERENCES `tb_supplier_con` (`sp_idx`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_consignment_return
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_consignment_return_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_consignment_return_item`;
CREATE TABLE `tb_consignment_return_item` (
  `rconit_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `rcon_code` varchar(11) NOT NULL DEFAULT '',
  `rconit_qty` decimal(8,2) DEFAULT NULL,
  `rconit_price` decimal(12,2) DEFAULT NULL,
  `rconit_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rconit_idx`,`it_code`,`rcon_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_consignment_return_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_customer`
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer`;
CREATE TABLE `tb_customer` (
  `cus_idx` int(11) NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(64) NOT NULL,
  `cus_pic` varchar(32) DEFAULT NULL,
  `cus_phone` varchar(64) DEFAULT NULL,
  `cus_handphone` varchar(64) DEFAULT NULL,
  `cus_fax` varchar(32) DEFAULT NULL,
  `cus_email` varchar(64) DEFAULT NULL,
  `cus_address` text,
  `cus_city` varchar(32) DEFAULT NULL,
  `cus_remark` text,
  PRIMARY KEY (`cus_idx`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customer
-- ----------------------------
INSERT INTO `tb_customer` VALUES ('9', 'Jaya Abadi', 'Budi', '', '', '', '', '', 'Jambi', '');

-- ----------------------------
-- Table structure for `tb_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_item`;
CREATE TABLE `tb_item` (
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `it_name` varchar(64) DEFAULT NULL,
  `it_desc` text,
  `it_has_ed` int(1) NOT NULL,
  `it_type` varchar(16) DEFAULT NULL,
  `it_qty` smallint(6) NOT NULL DEFAULT '1',
  `it_unit` varchar(8) DEFAULT NULL,
  `it_produced_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`it_code`),
  UNIQUE KEY `tb_item_it_code_idx` (`it_code`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_item
-- ----------------------------
INSERT INTO `tb_item` VALUES ('00000001', 'Panadol Flu & Batuk', 'Panadol Flu & Batuk', '1', 'medicine', '4', 'Qty', null);
INSERT INTO `tb_item` VALUES ('00000002', 'Panadol Sakit Kepala', 'Panadol Sakit Kepala', '1', 'medicine', '4', 'Qty', null);

-- ----------------------------
-- Table structure for `tb_item_category`
-- ----------------------------
DROP TABLE IF EXISTS `tb_item_category`;
CREATE TABLE `tb_item_category` (
  `itcat_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `cat_idx` int(11) NOT NULL,
  PRIMARY KEY (`itcat_idx`,`it_code`,`cat_idx`),
  KEY `tb_item_category_it_code_pk` (`it_code`),
  KEY `tb_item_category_cat_idx_pk` (`cat_idx`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_item_category
-- ----------------------------
INSERT INTO `tb_item_category` VALUES ('74', '123', '1');
INSERT INTO `tb_item_category` VALUES ('78', '0', '12');
INSERT INTO `tb_item_category` VALUES ('79', '0', '2');
INSERT INTO `tb_item_category` VALUES ('80', '0', '12');
INSERT INTO `tb_item_category` VALUES ('81', '0', '2');
INSERT INTO `tb_item_category` VALUES ('82', '0', '12');
INSERT INTO `tb_item_category` VALUES ('83', '0', '2');
INSERT INTO `tb_item_category` VALUES ('84', '0', '12');
INSERT INTO `tb_item_category` VALUES ('90', '123', '13');
INSERT INTO `tb_item_category` VALUES ('91', '123', '5');
INSERT INTO `tb_item_category` VALUES ('92', '123', '10');
INSERT INTO `tb_item_category` VALUES ('95', '123', '12');
INSERT INTO `tb_item_category` VALUES ('96', '123', '2');
INSERT INTO `tb_item_category` VALUES ('97', '123', '11');
INSERT INTO `tb_item_category` VALUES ('99', '123', '14');
INSERT INTO `tb_item_category` VALUES ('100', '123', '9');
INSERT INTO `tb_item_category` VALUES ('101', '123', '6');
INSERT INTO `tb_item_category` VALUES ('102', '123', '3');
INSERT INTO `tb_item_category` VALUES ('112', '00000002', '12');
INSERT INTO `tb_item_category` VALUES ('113', '00000002', '2');
INSERT INTO `tb_item_category` VALUES ('114', '00000002', '11');
INSERT INTO `tb_item_category` VALUES ('115', '00000001', '1');

-- ----------------------------
-- Table structure for `tb_item_price`
-- ----------------------------
DROP TABLE IF EXISTS `tb_item_price`;
CREATE TABLE `tb_item_price` (
  `ip_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL,
  `ip_date_from` date DEFAULT NULL,
  `ip_date_to` date DEFAULT NULL,
  `ip_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `ip_valid` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ip_idx`,`it_code`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_item_price
-- ----------------------------
INSERT INTO `tb_item_price` VALUES ('2', '12', '2012-03-25', null, '20000.00', '1');
INSERT INTO `tb_item_price` VALUES ('4', '123', '2012-03-26', null, '125000.00', '1');
INSERT INTO `tb_item_price` VALUES ('5', '123', '2012-03-01', '2012-03-25', '120000.00', '1');
INSERT INTO `tb_item_price` VALUES ('15', '00000001', '2012-03-26', null, '2200.00', '1');
INSERT INTO `tb_item_price` VALUES ('18', '00000002', '2012-03-01', '2012-03-09', '2150.00', '0');
INSERT INTO `tb_item_price` VALUES ('19', '00000002', '2012-03-10', null, '2250.00', '1');

-- ----------------------------
-- Table structure for `tb_purchasing`
-- ----------------------------
DROP TABLE IF EXISTS `tb_purchasing`;
CREATE TABLE `tb_purchasing` (
  `po_code` varchar(11) NOT NULL DEFAULT '',
  `po_date` date DEFAULT NULL,
  `sp_idx` int(11) NOT NULL DEFAULT '0',
  `po_sp_name` varchar(64) DEFAULT NULL,
  `po_discount` decimal(12,2) DEFAULT '0.00',
  `po_vat` decimal(3,2) DEFAULT '0.00',
  `po_sign_by` varchar(32) DEFAULT NULL,
  `po_last_updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `po_last_updated_by` varchar(16) DEFAULT NULL,
  `po_remark` text,
  PRIMARY KEY (`po_code`,`sp_idx`),
  UNIQUE KEY `tb_purchasing_po_code_idx` (`po_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_purchasing
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_purchasing_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_purchasing_item`;
CREATE TABLE `tb_purchasing_item` (
  `poit_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `po_code` varchar(11) NOT NULL DEFAULT '',
  `poit_qty` decimal(8,2) DEFAULT NULL,
  `poit_price` decimal(12,2) DEFAULT NULL,
  `poit_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`poit_idx`,`it_code`,`po_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_purchasing_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_sales`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales`;
CREATE TABLE `tb_sales` (
  `sl_code` varchar(11) NOT NULL DEFAULT '',
  `sl_date` date DEFAULT NULL,
  `cus_idx` int(11) NOT NULL DEFAULT '0',
  `sl_name` varchar(64) DEFAULT NULL,
  `sl_discount` decimal(12,2) DEFAULT '0.00',
  `sl_vat` decimal(3,2) DEFAULT '0.00',
  `sl_sign_by` varchar(32) DEFAULT NULL,
  `sl_last_updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `sl_last_updated_by` varchar(16) DEFAULT NULL,
  `sl_remark` text,
  PRIMARY KEY (`sl_code`,`cus_idx`),
  UNIQUE KEY `tb_purchasing_po_code_idx` (`sl_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sales
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_sales_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales_item`;
CREATE TABLE `tb_sales_item` (
  `slit_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `sl_code` varchar(11) NOT NULL DEFAULT '',
  `slit_qty` decimal(8,2) DEFAULT NULL,
  `slit_price` decimal(12,2) DEFAULT NULL,
  `slit_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`slit_idx`,`it_code`,`sl_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sales_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_sales_payment`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales_payment`;
CREATE TABLE `tb_sales_payment` (
  `slpay_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `sl_code` varchar(11) NOT NULL DEFAULT '',
  `slpay_date` date DEFAULT NULL,
  `slpay_method` varchar(16) DEFAULT NULL,
  `slpay_bank` varchar(16) DEFAULT NULL,
  `slpay_paid` decimal(12,2) DEFAULT NULL,
  `slpay_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`slpay_idx`,`it_code`,`sl_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sales_payment
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_sales_return`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales_return`;
CREATE TABLE `tb_sales_return` (
  `rsl_code` varchar(11) NOT NULL DEFAULT '',
  `sl_code` varchar(11) NOT NULL DEFAULT '',
  `rsl_date` date DEFAULT NULL,
  `cus_idx` int(11) NOT NULL DEFAULT '0',
  `rsl_name` varchar(64) DEFAULT NULL,
  `rsl_discount` decimal(12,2) DEFAULT '0.00',
  `rsl_vat` decimal(3,2) DEFAULT '0.00',
  `rsl_sign_by` varchar(32) DEFAULT NULL,
  `rsl_last_updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `rsl_last_updated_by` varchar(16) DEFAULT NULL,
  `rsl_remark` text,
  PRIMARY KEY (`rsl_code`,`sl_code`,`cus_idx`),
  UNIQUE KEY `tb_purchasing_po_code_idx` (`rsl_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sales_return
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_sales_return_item`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales_return_item`;
CREATE TABLE `tb_sales_return_item` (
  `rslit_idx` int(11) NOT NULL AUTO_INCREMENT,
  `it_code` varchar(10) NOT NULL DEFAULT '',
  `rsl_code` varchar(11) NOT NULL DEFAULT '',
  `rslit_qty` decimal(8,2) DEFAULT NULL,
  `rslit_price` decimal(12,2) DEFAULT NULL,
  `rslit_remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rslit_idx`,`it_code`,`rsl_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sales_return_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `tb_supplier`;
CREATE TABLE `tb_supplier` (
  `sp_idx` int(11) NOT NULL AUTO_INCREMENT,
  `sp_name` varchar(64) NOT NULL,
  `sp_pic` varchar(32) DEFAULT NULL,
  `sp_phone` varchar(64) DEFAULT NULL,
  `sp_handphone` varchar(64) DEFAULT NULL,
  `sp_fax` varchar(32) DEFAULT NULL,
  `sp_email` varchar(64) DEFAULT NULL,
  `sp_address` text NOT NULL,
  `sp_city` varchar(32) NOT NULL,
  `sp_remark` text,
  PRIMARY KEY (`sp_idx`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_supplier
-- ----------------------------
INSERT INTO `tb_supplier` VALUES ('4', 'Toko Serba Ada', '', '', '', '', '', 'Pulomas Ruko no 17', '', '');
INSERT INTO `tb_supplier` VALUES ('5', 'Indocore Perkasa', 'Nova', '4788 2599', '', '4788 2598', 'indocore@indocore.co.id', 'Graha Mas Pemuda Blok AB 19 Jl. Pemuda - Jakarta Timur', 'Jakarta', '');
INSERT INTO `tb_supplier` VALUES ('6', 'Medisindo Bahana', 'Asti', '425 0665', '', '425 0703', 'medisindo@medisindo.co.id', 'Rukan Graha Cempaka Mas Blok E15\r\nJl. Letjen Suprapto No. 1 Jakarta Pusat', 'Jakarta', '');

-- ----------------------------
-- Table structure for `tb_supplier_con`
-- ----------------------------
DROP TABLE IF EXISTS `tb_supplier_con`;
CREATE TABLE `tb_supplier_con` (
  `sp_idx` int(11) NOT NULL AUTO_INCREMENT,
  `sp_name` varchar(64) NOT NULL,
  `sp_pic` varchar(32) DEFAULT NULL,
  `sp_phone` varchar(64) DEFAULT NULL,
  `sp_handphone` varchar(64) DEFAULT NULL,
  `sp_fax` varchar(32) DEFAULT NULL,
  `sp_email` varchar(64) DEFAULT NULL,
  `sp_address` text,
  `sp_city` varchar(32) DEFAULT NULL,
  `sp_remark` text,
  PRIMARY KEY (`sp_idx`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_supplier_con
-- ----------------------------
INSERT INTO `tb_supplier_con` VALUES ('3', 'dsdasdasd', '', '', '', '', '', '', '', '');
